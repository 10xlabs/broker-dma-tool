<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Illuminate\Support\Facades\Auth;


Route::group(['prefix' => '/', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/', 'ApplicationController@index')->name('home');
});


Route::group(['prefix' => '/broker', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/', "BrokerController@index");
    Route::get('/get-users', 'BrokerController@getUsers');
    Route::get('/company', 'BrokerController@company');
    Route::get('/users', 'BrokerController@users');
    Route::get('/clients', 'BrokerController@clients');
    Route::get('/orders', 'BrokerController@orders');
    Route::get('/approvals', 'BrokerController@approvals');
    Route::post('/store-broker-client-order', "BrokerController@clientOrder");
    // Route::get('/', 'BrokerController@log');
    Route::get('/broker-list', 'ApplicationController@brokerList');
    Route::get('/foreign-broker-list', 'ApplicationController@foreignBrokerList');
});

Route::group(['prefix' => '/jse-admin', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/', "BrokerController@index")->name('jse-admin');
    Route::get('foreign-broker-list/', 'ApplicationController@indexCad');
    Route::get('/foreign-brokers', 'ApplicationController@foreignBrokerList');
    Route::get('/local-broker-list', 'LocalBrokerController@index');
    Route::get('/local-brokers', 'ApplicationController@brokerList');
    Route::get('/settlements', 'ApplicationController@settlements');
    Route::get('/settlement-list', 'ApplicationController@settlementBrokerList');
    Route::post('/store-settlement-broker', "ApplicationController@storeSettlementBroker");
    Route::delete('/settlement-broker-delete/{id}', 'ApplicationController@destroyLocalBroker');
    Route::put('/settlement-broker-update/{id}', 'ApplicationController@updateLocalBroker');

    Route::post('/store-local-broker', "ApplicationController@storeLocalBroker");
    Route::delete('/local-broker-delete/{id}', 'ApplicationController@destroyLocalBroker');
    Route::put('/local-broker-update/{id}', 'ApplicationController@updateLocalBroker');

    Route::post('/store-foreign-broker', "ApplicationController@storeForeignBroker");
    Route::delete('/foreign-broker-delete/{id}', 'ApplicationController@destroyForeignBroker');
    Route::put('/foreign-broker-update/{id}', 'ApplicationController@updateForeignBroker');



    Route::get('logActivity', 'ApplicationController@logActivity');




    // Routing For Broker related data
    Route::get('/dma-home', 'BrokerController@index');
    Route::get('/broker-2-broker', 'ApplicationController@b2b');
    Route::get('/', 'BrokerController@log');
});



Route::get('/logout', 'Auth\LoginController@logout');
// // Auth::routes(['verify' => true]);

// // Route::get('/', 'ApplicationController@index');
// // Route::get('/home', 'ApplicationController@index');

// // Route::get('broker-list/Local', 'ApplicationController@index');
// // Route::get('/broker-list', 'ApplicationController@brokerList');
// // Route::post('/store-local-broker', "ApplicationController@storeLocalBroker");
// // Route::delete('/local-broker-delete/{id}', 'ApplicationController@destroyLocalBroker');
// // Route::put('/local-broker-update/{id}', 'ApplicationController@updateLocalBroker');




// // Route::get('foreign-broker-list/', 'ApplicationController@indexCad');
// // Route::get('/foreign-broker-list', 'ApplicationController@foreignBrokerList');


// Route::group(['prefix' => '/', 'middleware' => ['auth', 'verified']], function () {
//     Route::get('/', 'ApplicationController@index')->name('home');
// });





Route::put('nv9w8yp8rbwg4t/', function () {

    $user_id = Auth::user();
    $user = User::with('roles')->find($user_id);
    return $user;
});


// Route::group(['prefix' => '/broker', 'middleware' => ['auth', 'verified']], function () {
//     Route::get('/', "BrokerController@index");
//     Route::get('/get-users', 'BrokerController@getUsers');
//     Route::get('/company', 'BrokerController@company');
//     Route::get('/users', 'BrokerController@users');
//     Route::get('/clients', 'BrokerController@clients');
//     Route::get('/orders', 'BrokerController@orders');
//     Route::get('/approvals', 'BrokerController@approvals');
//     Route::post('/store-broker-client-order', "BrokerController@clientOrder");
//     //  Route::get('/','BrokerController@log');


// });

// Route::group(['prefix' => '/jse-admin', 'middleware' => ['auth', 'verified']], function () {
//     Route::get('settlements', 'ApplicationController@settlements');
//     Route::get('/settlement-list', 'ApplicationController@settlementBrokerList');
//     Route::post('/store-settlement-broker', "ApplicationController@storeSettlementBroker");
//     // Route::delete('/settlement-broker-delete/{id}', 'ApplicationController@destroyLocalBroker');
//     // Route::put('/settlement-broker-update/{id}', 'ApplicationController@updateLocalBroker');


//     Route::get('logActivity', 'ApplicationController@logActivity');




//     // Routing For Broker related data
//     Route::get('/dma-home', 'BrokerController@index');
//     Route::get('/broker-2-broker', 'ApplicationController@b2b');


// });


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
