<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForeignBroker extends Model
{

    protected $guarded = ['id'];
    public function settlement_accounts()
    {
        return $this->hasMany('App\BrokerSettlementAccount');
    }
}
