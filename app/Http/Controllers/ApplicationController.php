<?php

namespace App\Http\Controllers;

use App\BrokerSettlementAccount;
use App\BrokerTradingAccount;
use App\ForeignBroker;
use App\Helpers\FunctionSet;
use App\Helpers\LogActivity;
use App\LocalBroker;
use App\User;
use App\Mail\SettlementAccountConfirmation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
        $this->LogActivity = new LogActivity;
        $this->HelperClass = new FunctionSet;
    }

    function index(Request $request)
    {
        $user = User::with('roles')->find(Auth::user());
        $user_role = $user[0]->roles[0]->name;

        switch ($user_role) {
            case 'broker':
                $local_brokers = LocalBroker::all();
                // return view('local')->with('local_brokers', $local_brokers);/
                return redirect('broker');
                break;
            case 'admin':
                return redirect('jse-admin');
                break;
        }
    }


    function indexCad()
    {
        $foreign_brokers = ForeignBroker::all();
        // return $foreign_brokers;
        return view('foreign')->with('foreign_brokers', $foreign_brokers);
    }



    function brokerList()
    {

        $local_brokers = LocalBroker::all();
        return $local_brokers;
    }

    function foreignBrokerList()
    {

        $foreign_brokers = ForeignBroker::all();
        return $foreign_brokers;
    }


    function settlementBrokerList()
    {

        $broker_settlement_accounts = BrokerSettlementAccount::with('local_broker', 'foreign_broker')->get();
        return $broker_settlement_accounts;
    }



    function storeLocalBroker(Request $request)
    {

        LogActivity::addToLog('Created New Local Broker');
        // $this->LogActivity($request);
        $broker = new LocalBroker;
        $broker->name = $request->name;
        $broker->email = $request->email;
        $broker->password = bcrypt('password');
        $broker->save();
    }


    function storeSettlementBroker(Request $request)
    {


        //Create A Broker Settlement Account
        $broker_settlement_account = new BrokerSettlementAccount;
        $broker_settlement_account->local_broker_id = $request->local_broker_id;
        $broker_settlement_account->foreign_broker_id = $request->foreign_broker_id;
        $broker_settlement_account->bank_name = $request->bank_name;
        $broker_settlement_account->account = $request->account;
        $broker_settlement_account->email = $request->email;
        $broker_settlement_account->status = 0;
        $broker_settlement_account->account_balance = '0.00';
        $broker_settlement_account->amount_allocated = '0.00';

        // $broker_settlement_account->account_balance = $request->account_balance;
        // $broker_settlement_account->amount_allocated = $request->amount_allocated;
        $broker_settlement_account->save();


        //Create Trading Account between both brokers

        $trading_account = new BrokerTradingAccount();
        $trading_account->local_broker_id = $request->local_broker_id;
        $trading_account->foreign_broker_id = $request->foreign_broker_id;
        $trading_account->umir = $request->umir;
        $trading_account->target_comp_id = $request->target_comp_id;
        $trading_account->sender_comp_id = $request->sender_comp_id;
        $trading_account->socket = $request->socket;
        $trading_account->port = $request->port;

        $trading_account->save();

        LogActivity::addToLog('Created New Settlement Account');
        LogActivity::addToLog('Created New Trading Account');
        // Mail::to($data['email'])->send(new SettlementAccountConfirmation($user));
        Mail::to($request->email)->send(new SettlementAccountConfirmation($request));
    }




    function storeForeignBroker(Request $request)
    {

        $broker = new ForeignBroker;
        $broker->name = $request->name;
        $broker->email = $request->email;
        $broker->save();
        LogActivity::addToLog('Created New Foreign Broker');
    }



    function destroyLocalBroker($id)
    {

        $b = LocalBroker::find($id);
        $b->delete();
        LogActivity::addToLog('Deleted Local Broker');
    }
    function destroyForeignBroker($id)
    {

        $b = ForeignBroker::find($id);
        $b->delete();
        LogActivity::addToLog('Deleted Foreign Broker');
    }



    function updateLocalBroker(Request $request, $id)
    {
        $broker               = LocalBroker::find($id);
        $broker->name         = $request->name;
        $broker->email        = $request->email;
        $broker->save();
    }

    function updateForeignBroker(Request $request, $id)
    {
        $broker               = ForeignBroker::find($id);
        $broker->name         = $request->name;
        $broker->email        = $request->email;
        $broker->save();
    }


    function settlements()
    {
        $local_brokers = LocalBroker::all();

        return view('accounts')->with('local_brokers', $local_brokers);
    }


    function brokerConfig()
    {
        $local_brokers = LocalBroker::all();

        return view('broker2broker')->with('local_brokers', $local_brokers);
    }

    function b2b()
    {
        $local_brokers = LocalBroker::all();

        return view('broker2broker')->with('local_brokers', $local_brokers);
    }
}
