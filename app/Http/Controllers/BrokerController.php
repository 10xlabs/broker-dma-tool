<?php

namespace App\Http\Controllers;

use App\BrokerClient;
use App\BrokerClientOrder;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class BrokerController extends Controller
{
    public function index()
    {
        return view('local');
    }

    function storeBrokerClient(Request $request)
    {


        // return $request;

        $broker_settlement_account = new BrokerSettlementAccount;
        $broker_settlement_account->local_broker_id = $request->local_broker_id;
        $broker_settlement_account->foreign_broker_id = $request->foreign_broker_id;
        $broker_settlement_account->bank_name = $request->bank_name;
        $broker_settlement_account->account = $request->account;
        $broker_settlement_account->email = $request->email;
        $broker_settlement_account->account_balance = '50000.00';
        $broker_settlement_account->amount_allocated = '1200.00';

        // $broker_settlement_account->account_balance = $request->account_balance;
        // $broker_settlement_account->amount_allocated = $request->amount_allocated;
        $broker_settlement_account->save();
        LogActivity::addToLog('Created New Settlement Account');
    }


    public function getUsers()
    {
        $data = User::all();
        return $data;
    }

    public function company()
    {
        return view('brokers.company');
    }



    public function users()
    {

        $broker_users = $data = BrokerClient::all();
        return view('brokers.user')->with('broker_users', $broker_users);
        // return view('brokers.client');
    }
    public function clients()
    {

        $broker_clients = $data = BrokerClient::all();
        return view('brokers.client')->with('broker_clients', $broker_clients);
        // return view('brokers.client');
    }
    public function orders()
    {
        // $orders = BrokerClientOrder::all();
        $data = DB::table('broker_client_orders')
            ->select('broker_client_orders.*', 'foreign_brokers.name as foreign_broker', 'local_brokers.name as local_broker')
            ->join('foreign_brokers', 'broker_client_orders.foreign_broker_id', 'foreign_brokers.id')
            ->join('local_brokers', 'broker_client_orders.local_broker_id', 'local_brokers.id')
            ->get();
        // returngit $data;

        return view('brokers.order')->with('orders', $data);
    }
    public function approvals()
    {
        return view('brokers.approval');
    }
    public function log()
    {
        return view('brokers.log');
    }


    public function clientOrder(Request $request)
    {
        return "Hello World";
    }
}
