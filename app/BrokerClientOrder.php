<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrokerClientOrder extends Model
{
    protected $guarded = ['id'];
}
