<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="This document is intended to act as a guideline for deployments of the LARAVEL / VUE Application Environment On (AWS)">
    <meta name="author" content="Jason Lawrence Innovate 10x">
        <meta name="csrf-token" content="{{ csrf_token() }}">   
        <title>JSE Broker DMA Tool</title>
        @yield('link-styles')
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/app.css')}}" rel="stylesheet">
        
    </head>
    <body>
        <div id="app">
        @yield('content')
        
        </div>
        <script src="{{ asset('js/app.js')}}"></script>
     
    </body>
</html>
