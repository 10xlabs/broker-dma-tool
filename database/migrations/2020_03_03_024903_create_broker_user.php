<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrokerUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broker_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('local_broker_id')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->foreign('local_broker_id')->references('id')->on('local_brokers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void 
     */
    public function down()
    {
        Schema::dropIfExists('broker_users');
    }
}
