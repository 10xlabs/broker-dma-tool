<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrokerClientOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broker_client_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('local_broker_id')->unsigned();
            $table->integer('foreign_broker_id')->unsigned();
            $table->text('handling_instructions');
            $table->integer('order_quantity');
            $table->string('order_type');
            $table->string('order_status');
            $table->date('order_date');
            $table->string('currency');
            $table->string('symbol');
            $table->integer('price');
            $table->smallInteger('quantity');
            $table->string('country');
            $table->string('status_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('broker_client_orders');
    }
}
