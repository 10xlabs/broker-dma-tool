<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $role_broker = new Role();
      $role_broker->name = 'broker';
      $role_broker->description = 'Broker Account';
      $role_broker->save();

      $role_admin = new Role();
      $role_admin->name = 'admin';
      $role_admin->description = 'Administrative User';
      $role_admin->save();
    }
}
