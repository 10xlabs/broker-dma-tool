<?php

use App\BrokerSettlementAccount;
// use App\SettlementAccount;

use Illuminate\Database\Seeder;
use Twilio\Rest\Accounts;

class SettlementAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        BrokerSettlementAccount::insert([
            [
                'local_broker_id' => 1,
                'foreign_broker_id' => 1,
                'bank_name' => 'RBC',
                'account' => 'RBC-10272',
                'status' => 0,
                'email' => 'RBC@gmail.com',
                'account_balance' => '0.00',
                'amount_allocated' => '0.00',
                
            ],
            // [
            //     'local_broker_id' => 2,
            //     'foreign_broker_id' => 2,
            //     'bank_name' => 'WF',
            //     'Account' => '',
            //     'email' => 'wellsfargo@gmail.com',
            //     'account_balance' => '20000.00',
            //     'amount_allocated' => '3000.00',
                
            // ]
        ]);
    }
}
