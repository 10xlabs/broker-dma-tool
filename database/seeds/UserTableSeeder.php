<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_broker = Role::where('name', 'broker')->first();
        $role_admin  = Role::where('name', 'admin')->first();
        $broker = new User();
        $broker->name = 'JMMB Broker 1';
        $broker->email = 'local_broker@JMMB.com';
        $broker->password = bcrypt('password');
        $broker->save();

        $broker->roles()->attach($role_broker);

        $admin = new User();
        $admin->name = 'JSE DMA Admin';
        $admin->email = 'admin@innovate10x.com';
        $admin->password = bcrypt('Secur!ty12310x');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
