<?php

use App\LocalBroker;
use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

class LocalBrokerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        LocalBroker::insert([
            [
            'name' =>  'JMMB',
            'email' => 'broker@jmmb.com',
            // 'email_verified_at' => now(), //For future use
            'password' => bcrypt('password')
            ],
            [
            'name' =>  'Bartia',
            'email' => 'broker@barita.com',
            // 'email_verified_at' => now(), //For future use
            'password' => bcrypt('password')
            ],
            [
            'name' =>  'SAGICOR',
            'email' => 'broker@sagicor.com',
            // 'email_verified_at' => now(), //For future use
            'password' => bcrypt('password')
            ]
        ]);
    }
}
